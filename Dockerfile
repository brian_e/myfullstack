FROM python:3.6-onbuild

ENV PYTHONUNBUFFERED 1

RUN apt-get update && apt-get install -y --no-install-recommends \
        lsof \
        vim \
    && rm -rf /var/lib/apt/lists/*
