import random
import sys
import lorem

import django
django.setup()

from app.models import User, Note

if not sys.argv[1:]:
    print('specify an email')
    sys.exit()

author = User.objects.get(email=sys.argv[1])
Note.objects.create(author=author,
                    contents=lorem.paragraph(),
                    visibility=random.choice(['private', 'public']))
