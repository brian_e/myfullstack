from functools import partial
import json
import logging
from django.contrib.auth import authenticate, login, logout, get_user_model
from django.contrib.auth.decorators import login_required
from django.core.serializers import serialize
from django.core.serializers.json import DjangoJSONEncoder
from django.core.exceptions import ValidationError
from django.http import JsonResponse
from django.template.response import TemplateResponse
from django.utils.decorators import method_decorator
from django.views.generic import View
from .models import Note


logger = logging.getLogger('app')
to_dict = partial(serialize, 'python')


def get_state(user):
    public_notes = Note.objects.filter(visibility='public')
    if user.is_authenticated:
        email = user.email
        my_notes = Note.objects.filter(author=user).order_by('-updated_at')[:7]
        public_notes = public_notes.exclude(author=user)
    else:
        email = ''
        my_notes = []

    return {'isAuthenticated': bool(user.is_authenticated),
            'email': email,
            'myNotes': to_dict(my_notes),
            'publicNotes': to_dict(public_notes.order_by('-updated_at')[:7])}


class IndexView(View):
    def get(self, request, *args, **kwargs):
        state = json.dumps(get_state(request.user), cls=DjangoJSONEncoder)
        return TemplateResponse(request, 'index.html', {'initial_state': state})


class LoginRequired:
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class RegisterView(View):
    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            data = json.loads(request.body)
            if not {'email', 'password'}.issubset(data):
                return JsonResponse({'message': 'email and password required'},
                                    status=400)

            user = get_user_model()(email=data['email'])
            user.set_password(data['password'])
            try:
                user.full_clean()
            except ValidationError as e:
                return JsonResponse({'message': ' '.join(e.messages)}, status=400)
     
            user.save()
            login(request, user)
        return JsonResponse(get_state(request.user))


class LoginView(View):
    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            data = json.loads(request.body)
            if not {'email', 'password'}.issubset(data):
                return JsonResponse({'message': 'email and password required'},
                                    status=400)

            user = authenticate(email=data['email'], password=data['password'])
            if user is None:
                return JsonResponse({'message': 'authentication failed'},
                                    status=401)

            login(request, user)

        return JsonResponse(get_state(request.user))


class LogoutView(LoginRequired, View):
    def post(self, request, *args, **kwargs):
        logout(request)
        return JsonResponse(get_state(request.user))


class NotesView(LoginRequired, View):
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        Note.objects.create(author=request.user, contents=data['note'])
        return JsonResponse({}, status=201)
