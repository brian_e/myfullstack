from django.contrib import auth
from django.db import models


class User(auth.models.AbstractBaseUser):
    email = models.EmailField(max_length=255, unique=True)
    objects = auth.models.BaseUserManager()
    USERNAME_FIELD = 'email'


class Note(models.Model):
    author = models.ForeignKey(User)
    contents = models.TextField()
    visibility = models.CharField(max_length=16,
                                  choices=(('public', 'public'),
                                           ('friends', 'friends'),
                                           ('private', 'private'),),
                                  default='private')
    updated_at = models.DateTimeField(auto_now=True)
