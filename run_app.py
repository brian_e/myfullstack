import cherrypy

import django
django.setup()

from django.core.wsgi import get_wsgi_application

cherrypy.config.update({'engine.autoreload_on': True,
                        'log.screen': True,
                        'server.socket_port': 4243,
                        'server.socket_host': '0.0.0.0'})
cherrypy.tree.graft(get_wsgi_application(), '/')
cherrypy.engine.start()
cherrypy.engine.block()
