from django.conf.urls import url
from app import views

urlpatterns = [
    url(r'^$', views.IndexView.as_view()),
    url(r'^api/register$', views.RegisterView.as_view()),
    url(r'^api/login$', views.LoginView.as_view()),
    url(r'^api/logout$', views.LogoutView.as_view()),
    url(r'^api/notes/$', views.NotesView.as_view()),
]
