import axios from 'axios';
import React from 'react';
import ReactDOM from 'react-dom';


class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {buttonText: 'login',
                  linkText: 'register',
                  email: '',
                  password: ''};
  }

  componentDidMount() {
    this.emailInput.focus();
  }

  swapText = () => {
    this.setState({buttonText: this.state.linkText, linkText: this.state.buttonText});
  }

  handleEmailChange = (event) => {
    this.setState({email: event.target.value});
  }

  handlePasswordChange = (event) => {
    this.setState({password: event.target.value});
  }

  handlePasswordKeyPress = (event) => {
    if (event.key === 'Enter') {
      this.submit();
    }
  }

  submit = () => {
    if (this.state.buttonText === 'login') {
      this.props.login(this.state.email, this.state.password);
    } else {
      this.props.register(this.state.email, this.state.password);
    }
  }

  render() {
    return (
      <span>
        <input type="email"
               placeholder="email"
               value={this.state.email}
               onChange={this.handleEmailChange}
               ref={x => this.emailInput = x} />
        <span> </span>
        <input type="password"
               placeholder="password"
               value={this.state.password}
               onChange={this.handlePasswordChange} 
               onKeyPress={this.handlePasswordKeyPress}/>
        <span> </span>
        <input type="button"
               value={this.state.buttonText}
               onClick={this.submit} />
        <span> (or, <a href="#" onClick={this.swapText}>{this.state.linkText}</a>)</span>
      </span>
    );
  }
}

class NotesPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {note: ''};
  }

  handleNoteChange = (event) => {
    this.setState({note: event.target.value});
  }

  submit = () => {
    axios.post('/api/notes/', {note: this.state.note})
      .then(res => {
      });
  }

  render() {
    return (
      <div>
        {this.props.isAuthenticated &&
          <span>
            <input type="text"
                   placeholder="note"
                   value={this.state.note}
                   onChange={this.handleNoteChange} />
            <span> </span>
            <input type="button"
                   value="Submit"
                   onClick={this.submit} />
          </span>
        }
        {this.props.myNotes.length > 0 &&
          <div>
            <b>My notes</b>
            <ul>
              {this.props.myNotes.map(n => <li key={n.pk}>{n.fields.contents}</li>)}
            </ul>
          </div>
        }
        {this.props.publicNotes.length > 0 &&
          <div>
            <b>Public notes</b>
            <ul>
              {this.props.publicNotes.map(n => <li key={n.pk}>{n.fields.contents}</li>)}
            </ul>
          </div>
        }
      </div>
    );
  }
}


class Index extends React.Component {
  constructor(props) {
    super(props);
    this.state = window.initialState;
  }

  register = (email, password) => {
    axios.post('/api/register', {email, password})
      .then(res => {
        this.setState(res.data);
      });
  }

  login = (email, password) => {
    axios.post('/api/login', {email, password})
      .then(res => {
        this.setState(res.data);
      });
  }

  logout = () => {
    axios.post('/api/logout')
      .then(res => {
        this.setState(res.data);
      });
  }

  render() {
    return (
      <div>
        <div>
          <span> ☕ &nbsp;</span>
          {this.state.isAuthenticated ?
            <span>
              <a href="#" onClick={this.logout}>logout</a>
              <span> ({this.state.email})</span>
            </span> :
            <LoginForm register={this.register}
                       login={this.login} />}
        </div>
        <NotesPage isAuthenticated={this.state.isAuthenticated}
                   publicNotes={this.state.publicNotes}
                   myNotes={this.state.myNotes}/>
      </div>
    );
  }
}


ReactDOM.render(<Index />, document.getElementById('root'));
