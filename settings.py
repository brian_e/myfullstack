import os

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
SECRET_KEY = 'a'
DEBUG = True
AUTH_USER_MODEL = 'app.User'
ROOT_URLCONF = 'urls'

INSTALLED_APPS = [
    'django.contrib.contenttypes',
    'django.contrib.auth',
    'app'
]

DATABASES = {'default': {
    'ENGINE': 'django.db.backends.postgresql',
    'HOST': 'db',
    'NAME': 'postgres',
    'USER': 'postgres'}}

MIDDLEWARE = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
]

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://redis:6379/0",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}

SESSION_ENGINE = "django.contrib.sessions.backends.cache"
SESSION_CACHE_ALIAS = "default"

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
    },
]

CSRF_COOKIE_NAME = 'XSRF-TOKEN'
CSRF_HEADER_NAME = 'HTTP_X_XSRF_TOKEN'  # 'X-XSRF-TOKEN'

LOGGING = {
    'disable_existing_loggers': False,
    'handlers': {
        'console': {'level': 'DEBUG', 'class': 'logging.StreamHandler'}},
    'loggers': {
        'app': {'handlers': ['console'], 'level': 'DEBUG'},
        'django.db.backends': {'handlers': ['console'], 'level': 'DEBUG'},
        'django.request': {'handlers': ['console'], 'level': 'DEBUG'},},
    'version': 1}
